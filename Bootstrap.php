<?php

namespace txd;

use Yii;
use yii\base\BootstrapInterface;
use yii\web\Application;

/**
 * This class participates in the application bootstrap process.
 *
 * @author Tuxido <hello@tuxido.ro>
 */
class Bootstrap implements BootstrapInterface
{
	/**
	 * @inheritdoc
	 */
	public function bootstrap($app)
	{
		Yii::setAlias('@txd', __DIR__);

		$this->registerTranslations();
	}

	/**
	 * Registers i18n translations.
	 */
	public function registerTranslations()
	{
		Yii::$app->i18n->translations['txd'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'sourceLanguage' => 'en-US',
			'basePath' => '@txd/messages',
		];
	}
}
