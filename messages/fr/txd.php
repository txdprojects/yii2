<?php
/**
 * Translation map for fr-FR
 */
return [
	'Create' => 'Créer',
	'View' => 'Vue',
	'Update' => 'Mise à jour',
	'Delete' => 'Effacer',
];
