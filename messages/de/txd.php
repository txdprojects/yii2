<?php
/**
 * Translation map for de-DE
 */
return [
	'Create' => 'Erstellen',
	'View' => 'Aussicht',
	'Update' => 'Aktualisieren',
	'Delete' => 'Löschen',
];
