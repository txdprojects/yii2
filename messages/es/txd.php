<?php
/**
 * Translation map for es-ES
 */
return [
	'Create' => 'Crear',
	'View' => 'Ver',
	'Update' => 'Actualizar',
	'Delete' => 'Eliminar',
];
