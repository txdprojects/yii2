<?php
/**
 * Translation map for en-US
 */
return [
	'Create' => 'Create',
	'View' => 'View',
	'Update' => 'Update',
	'Delete' => 'Delete',
];
