<?php
/**
 * Translation map for ro-RO
 */
return [
	'Create' => 'Adăugare',
	'View' => 'Vizualizare',
	'Update' => 'Actualizare',
	'Delete' => 'Ștergere',
];
