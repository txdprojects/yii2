<?php

namespace txd\helpers;

/**
 * {@inheritdoc}
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class StringHelper extends \yii\helpers\StringHelper
{
	/**
	 * Generates a random string based of an integer length.
	 *
	 * @param int $length The number of characters.
	 * @param int|null $type The type of characters.
	 * @param string|array|null $source The source from where the string should be generated.
	 * @return string The generated random string.
	 */
	public static function generateRandomString($length, $type = null, $source = null)
	{
		if (!$source) {
			if ($type === 'numeric') {
				$source = '0123456789';
			} elseif ($type === 'alpha') {
				$source = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			} elseif (!$source || !$type || $type === 'alphanumeric') {
				$source = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			}
		} elseif (is_array($source)) {
			$source = implode('', $source);
		}

		return substr(str_shuffle(str_repeat($source, ceil($length / strlen($source)))), 1, $length);
	}

	/**
	 * Extract initials from a given string.
	 *
	 * @param string $source The source from where letters should be generated.
	 * @param int $length The exact number of the initials.
	 * @return string|null The extracted initials.
	 */
	public static function extractInitials($source, $length = null)
	{
		if (!is_string($source) || strlen($source) == 0) {
			return null;
		}

		$initials = [];
		if (!($sourceParts = preg_split('/\s+/', $source))) {
			$sourceParts = [$source];
		}
		if ($length > 0) {
			// Split by letters if there is only one word
			if (count($sourceParts) === 1) {
				preg_match_all('/[a-zA-Z]/', $source, $matches);
				$sourceParts = $matches[0];
			}
		}
		// Extract the first letter of each part
		foreach ($sourceParts as $sourcePart) {
			$initials[] = substr($sourcePart, 0, 1);
		}
		if ($length > 0) {
			$initials = array_slice($initials, 0, $length);
		}

		return implode('', $initials);
	}
}
