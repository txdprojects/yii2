<?php

namespace txd\helpers;

/**
 * File system helper.
 *
 * @author Tuxido <hello@tuxido.ro>
 */
class FileHelper extends \yii\helpers\FileHelper
{
	/**
	 * Creates an operating system independent symbolic link.
	 *
	 * @param string|array $target a single link target or an array of multiple target => link pairs.
	 * @param string|null $link the link name, it will be ignored if the $target is array.
	 * @return bool the success or the failure of the symlink creation.
	 */
	public static function symlink($target, $link = null)
	{
		if (is_array($target)) {
			foreach ($target as $key => $val) {
				if (DIRECTORY_SEPARATOR === '\\') {
					exec("mklink /D " . static::normalizePath("{$val} {$key}"));
				} else {
					@symlink($key, $val);
				}
			}
			return true;
		}

		if (DIRECTORY_SEPARATOR === '\\') {
			exec("mklink /D " . static::normalizePath("{$link} {$target}"));
		} else {
			@symlink($target, $link);
		}
		return true;
	}

	/**
	 * Maps the content of a file to an associative array.
	 *
	 * @link https://stackoverflow.com/a/41942299/13071967
	 *
	 * @param string $file The file path.
	 * @param string $delimiter A delimiter which splits the file data. Defaults to comma (for CSV files).
	 * @return array
	 */
	public static function mapFileToArray($file, $delimiter = ',')
	{
		$rows = array_map(function ($row) use ($delimiter) {
			return str_getcsv($row, $delimiter);
		}, file($file));
		$header = array_shift($rows);
		$data = [];

		foreach ($rows as $row) {
			$data[] = array_combine($header, $row);
		}

		return $data;
	}
}
