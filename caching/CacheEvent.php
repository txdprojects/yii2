<?php

namespace txd\caching;

use yii\base\Event;

/**
 * CacheEvent represents the information available when the cache is flushed.
 *
 * @author Tuxido <hello@tuxido.ro>
 */
class CacheEvent extends Event
{
	/**
	 * @var string The cache key to be invalidated.
	 */
	public $key;
}
